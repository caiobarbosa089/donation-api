module.exports = {
	haltOnTimedout: (req, res, next) => {
    if (!req.timedout) next();
  }
}
