const express = require('express');
const petController = require('../controllers/pet');
const router = express.Router();

router.get('/', petController.getPet);
router.post('/', petController.postPet);
router.put('/', petController.putPet);
router.delete('/', petController.deletePet);
router.get('/all', petController.getAllPet);
router.get('/donor', petController.getDonorPet);

module.exports = router; 
