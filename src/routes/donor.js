const express = require('express');
const donorController = require('../controllers/donor');
const router = express.Router();

router.get('/', donorController.getDonor);
router.post('/', donorController.postDonor);
router.put('/', donorController.putDonor);
router.delete('/', donorController.deleteDonor);
router.get('/all', donorController.getAllDonor);

module.exports = router; 
