const mongoose = require('mongoose');

const petSchema = mongoose.Schema({
	_id: mongoose.Schema.Types.ObjectId,
	donor_id: String,
    name: String,
    age: Number,
    bread: String,
    species: String,
    gender: String
}, {
	versionKey: false // Unable auto-version after persist database
});

const Pet = mongoose.model('pets', petSchema);

module.exports = Pet;
