const mongoose = require('mongoose');

const donorSchema = mongoose.Schema({
	_id: mongoose.Schema.Types.ObjectId,
	name: String,
    contact: String,
    adress: String,
}, {
	versionKey: false // Unable auto-version after persist database
});

const Donor = mongoose.model('donors', donorSchema);

module.exports = Donor;
