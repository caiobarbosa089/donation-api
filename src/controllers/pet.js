const mongoose = require('mongoose');
const Pet = require('../models/pet');

exports.getAllPet = async (req, res, next) => {
    try {
        // Pagination filters
        const page = parseInt(req.query.page || 1);
        const limit = parseInt(req.query.limit || 100);
        const skip = (page - 1) * limit;
        const pagination = { page, limit, skip };
        const results = await Pet.find({}, (err, pets) => {
            // Error returned
            if (err) return res.status(400).json({ success: false, error: 'Invalid request, something went wrong!' });
            // Invalid data received
            if (!pets) return res.status(400).json({ success: false, error: 'Unauthorized action!' });
        })
        .skip(skip)
        .limit(limit);
        // BD connection
        // Get total page
        pagination['total'] = await Pet.find({}, (err, pets) => { }).countDocuments();
        // Everything OK
        return res.status(200).json({success: true, results, pagination}); // Return collection saved including the MongoDB _id
    } catch (error) {
        // Something wrong
        return res.status(500).send({ success: false, error: error });
    }
};

exports.getDonorPet = async (req, res, next) => {
    try {
        // donor_id
        const donor_id = req.query.donor_id;
        // Pagination filters
        const page = parseInt(req.query.page || 1);
        const limit = parseInt(req.query.limit || 100);
        const skip = (page - 1) * limit;
        const pagination = { page, limit, skip };
        // BD connectiondonor_id
        const results = await Pet.find({donor_id}, (err, pets) => {
            // Error returned
            if (err) return res.status(400).json({ success: false, error: 'Invalid request, something went wrong!' });
            // Invalid data received
            if (!pets) return res.status(400).json({ success: false, error: 'Unauthorized action!' });
        })
        .skip(skip)
        .limit(limit);
        // BD connection
        // Get total page
        pagination['total'] = await Pet.find({}, (err, pets) => { }).countDocuments();
        // Everything OK
        return res.status(200).json({success: true, results, pagination}); // Return collection saved including the MongoDB _id
    } catch (error) {
        // Something wrong
        return res.status(500).send({ success: false, error: error });
    }
};

exports.getPet = async (req, res, next) => {
    try {
        // Query params
        const _id = req.query.id || null;
        // Invalid params
        if(!_id )return res.status(400).json({ success: false, error: 'No id' });
        // Invalid Id
        if(!mongoose.Types.ObjectId.isValid(_id)) return res.status(400).json({ success: false, error: 'Invalid id' });
        // BD connection
        await Pet.findById(_id, (err, pet) => {
            // Error returned
            if (err) return res.status(400).json({ success: false, error: 'Invalid request, something went wrong!' });
            // Invalid data received
            if (!pet) return res.status(400).json({ success: false, error: 'Unauthorized action!' });
            // Everything OK
            return res.status(200).json({success: true, results: pet}); // Return collection saved including the MongoDB _id
        })
    } catch (error) {
        // Something wrong
        return res.status(500).send({ success: false, error: error });
    }
};

exports.postPet = async (req, res, next) => {
    try {
        // Body
        const petBody = req.body;
        // Invalid data received
        if (Object.keys(petBody).length == 0) return res.status(400).json({ success: false, error: 'No body' });
        // Body needed
        const { donor_id, name, age, bread, species, gender } = petBody;
        const data = { donor_id, name, age, bread, species, gender };
        // Invalid donor_id
        if(!mongoose.Types.ObjectId.isValid(donor_id)) return res.status(400).json({ success: false, error: 'Invalid donor_id' });
        // Generating new MongoDB _ID
        const _id = mongoose.Types.ObjectId();
        // BD connection
        Pet.create({ _id, ...data }, (err, pet) => {
            // Error returned
            if (err) return res.status(400).json({ success: false, error: 'Invalid request, something went wrong!', err });
            // Everything OK
            return res.status(201).json({ success: true, results: pet });
        });
    } catch (error) {
        // Something wrong
        return res.status(500).send({ success: false, error: error });
    }
};

exports.putPet = async (req, res, next) => {
    try {
        // Body
        const petBody = req.body;
        // Invalid data received
        if (Object.keys(petBody).length == 0) return res.status(400).json({ success: false, error: 'No body' });
        // Body needed
        const { _id, donor_id, name, age, bread, species, gender } = petBody;
        const data = { donor_id, name, age, bread, species, gender };
        // Check mongo's ids
        if(!mongoose.Types.ObjectId.isValid(_id)) return res.status(400).json({ success: false, error: 'Invalid id' });
        if(!mongoose.Types.ObjectId.isValid(donor_id)) return res.status(400).json({ success: false, error: 'Invalid id' });
        // BD connection
        // Check if exists
        const finded = await Pet.findById(_id, (err, pet) => {
            // Error returned
            if (err) return res.status(400).json({ success: false, error: 'Invalid request, something went wrong!', err });
        });
        // Id not found
        if(!finded) return res.status(400).json({ success: false, error: 'Id not found' });
        // BD connection
        Pet.findByIdAndUpdate(_id, { ...data }, (err, pet) => {
            // Error returned
            if (err) return res.status(400).json({ success: false, error: 'Invalid request, something went wrong!', err });
            // Everything OK
            return res.status(200).json({ success: true, results: pet });
        });
    } catch (error) {
        // Something wrong
        return res.status(500).send({ success: false, error: error });
    }
};

exports.deletePet = async (req, res, next) => {
    try {
        // Query params
        // MongoDB _ID 
        const _id = req.query.id || null;
        // Invalid params
        if(!_id )return res.status(400).json({ success: false, error: 'Source required to perform the search!' });
        // Invalid Id
        if(!mongoose.Types.ObjectId.isValid(_id)) return res.status(400).json({ success: false, error: 'Invalid id' });
        // BD connection
        // Check if exists
        const finded = await Pet.findById(_id, (err, pet) => {
            // Error returned
            if (err) return res.status(400).json({ success: false, error: 'Invalid request, something went wrong!', err });
        });
        // Id not found
        if(!finded) return res.status(400).json({ success: false, error: 'Id not found' });
        // Remove pet by it's _ID
        Pet.deleteOne({ _id }, err => {
            // Something wrong happens
            if (err) return res.status(400).json({ success: false, error: 'Can\'t remove pet!' });
            // Everything OK
            return res.status(200).json({ success: true });
        });

    } catch (error) {
        // Something wrong
        return res.status(500).send({ success: false, error: error });
    }
};