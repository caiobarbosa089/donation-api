const express = require('express');
const bodyParser = require('body-parser');
const timeout = require('connect-timeout')
const mongoose = require('mongoose');
const compression = require('compression');
const port = (process.env.NODE_ENV === 'test') ? (process.env.PORT_TEST || 2001) : (process.env.PORT || 2000);

// Express application
const app = express();

// Use this url to connect in docker MongoDB 
const mongoConnection = `mongodb://${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}@${process.env.MONGO_HOSTNAME}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}?authSource=admin`;
// Use this url to connect in local MongoDB 
const url = 'mongodb://root:rootpassword@localhost:27017/donation?authSource=admin';
// MongoDB (Mongoose) connection
const db = mongoose.connect(mongoConnection, { useNewUrlParser: true, useUnifiedTopology: true });

// MongoDB (Mongoose) config
mongoose.set('useFindAndModify', false);

// Event interceptors if you want to check status
mongoose.connection.on('connected', () => console.log('MongoDB (Mongoose) - Connected'));
mongoose.connection.on('error', error => console.log('MongoDB (Mongoose) - Error', error));
mongoose.connection.on('disconnected', () => console.log('MongoDB (Mongoose) - Disconnected'));

// App config
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(timeout(process.env.DEFAULT_TIMEOUT || '30s'));

// Middleware compress
app.use(compression({ filter: require('./middlewares/compress').shouldCompress }))
// Middleware timeout
app.use(require('./middlewares/haltOnTimedout').haltOnTimedout);
// Middleware allowing all requests headers
app.use(require('./middlewares/cors').allowAll);

// Assigning routes to imported routers
app.use('/', require('./routes/swagger'));
app.use('/donor/', require('./routes/donor'));
app.use('/pet/', require('./routes/pet'));

// Return Not Found for any route not mapped
app.use((req, res) => res.status(404).json({ error: 'Not found' }));

// Default error handler - Detect any error in Express and handle it in middleware
app.use((err, req, res, next) => {
	// Runtime error output in server terminal
	console.error('ERROR ', err);
	// Send generic error message under 500 status response
	res.status(500).json({ success: false, error: 'Something went wrong! Verify the URL you tried to access and if you are filling the parameters correctly if they are needed.' });
});

// Restart app on crash
process.on('uncaughtException', err => {})

// Starting application server listening to it's port
app.listen(port, () => console.log(`Server listening to port ${port}`));

// Export app
module.exports = app;