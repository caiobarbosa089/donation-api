// Import the dependencies for testing
const app = require('../../index');
const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');

// Configure chai
chai.use(chaiHttp);
chai.should();

describe('Pet', () => {
  describe('POST /', () => {
    it('should post a pet', (done) => {
      const body = {
        donor_id: mongoose.Types.ObjectId(),
        name: "string",
        age: 0,
        bread: "string",
        species: "string",
        gender: "string"
      };
      chai.request(app)
        .post('/pet')
        .send(body)
        .end((err, res) => {
          res.should.have.status(201);
          res.body.should.be.a('object');
          done();
        });
    });

    it('should not post a pet - invalid donor_id', (done) => {
      const body = {
        donor_id: 'invalid',
        name: "string",
        age: 0,
        bread: "string",
        species: "string",
        gender: "string"
      };
      chai.request(app)
        .post('/pet')
        .send(body)
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });

    it('should not post a pet - null donor_id', (done) => {
      const body = {
        donor_id: null,
        name: "string",
        age: 0,
        bread: "string",
        species: "string",
        gender: "string"
      };
      chai.request(app)
        .post('/pet')
        .send(body)
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });

    it('should not post a pet - body = { }', (done) => {
      chai.request(app)
        .post('/pet')
        .send({ })
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });

    it('should not post a pet - body = [ ]', (done) => {
      chai.request(app)
        .post('/pet')
        .send([ ])
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });
    
    it('should not post a pet - body = null', (done) => {
      chai.request(app)
        .post('/pet')
        .send(null)
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });
  });
});