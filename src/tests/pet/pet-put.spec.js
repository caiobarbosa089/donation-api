// Import the dependencies for testing
const app = require('../../index');
const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');

// Configure chai
chai.use(chaiHttp);
chai.should();

describe('Pet', () => {
  describe('PUT /', () => {
    it('should put a pet', (done) => {
      const body = {
        _id: '5fdab997c89cca3c7cb0c3bc', // valid mongo_id
        donor_id: mongoose.Types.ObjectId(),
        name: "string",
        age: 0,
        bread: "string",
        species: "string",
        gender: "string"
      };
      chai.request(app)
        .put('/pet')
        .send(body)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          done();
        });
    });

    it('should not put a pet - id not created', (done) => {
      const body = {
        _id: mongoose.Types.ObjectId(),
        donor_id: mongoose.Types.ObjectId(),
        name: "string",
        age: 0,
        bread: "string",
        species: "string",
        gender: "string"
      };
      chai.request(app)
        .put('/pet')
        .send(body)
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });

    it('should not put a pet - invalid id and donor_id', (done) => {
      const body = {
        _id: 'invalid',
        donor_id: 'invalid',
        name: "string",
        age: 0,
        bread: "string",
        species: "string",
        gender: "string"
      };
      chai.request(app)
        .put('/pet')
        .send(body)
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });

    it('should not put a pet - invalid id', (done) => {
      const body = {
        _id: 'invalid',
        donor_id: mongoose.Types.ObjectId(),
        name: "string",
        age: 0,
        bread: "string",
        species: "string",
        gender: "string"
      };
      chai.request(app)
        .put('/pet')
        .send(body)
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });

    it('should not put a pet - invalid donor_id', (done) => {
      const body = {
        _id: '5fdab997c89cca3c7cb0c3bc', // valid mongo_id
        donor_id: 'invalid',
        name: "string",
        age: 0,
        bread: "string",
        species: "string",
        gender: "string"
      };
      chai.request(app)
        .put('/pet')
        .send(body)
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });

    it('should not put a pet - null id and donor_id', (done) => {
      const body = {
        _id: null,
        donor_id: null,
        name: "string",
        age: 0,
        bread: "string",
        species: "string",
        gender: "string"
      };
      chai.request(app)
        .put('/pet')
        .send(body)
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });

    it('should not put a pet - null id', (done) => {
      const body = {
        _id: null,
        donor_id: mongoose.Types.ObjectId(),
        name: "string",
        age: 0,
        bread: "string",
        species: "string",
        gender: "string"
      };
      chai.request(app)
        .put('/pet')
        .send(body)
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });

    it('should not put a pet - null donor_id', (done) => {
      const body = {
        _id: '5fdab997c89cca3c7cb0c3bc', // valid mongo_id
        donor_id: null,
        name: "string",
        age: 0,
        bread: "string",
        species: "string",
        gender: "string"
      };
      chai.request(app)
        .put('/pet')
        .send(body)
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });

    it('should not put a pet - body = { }', (done) => {
      chai.request(app)
        .put('/pet')
        .send({ })
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });

    it('should not put a pet - body = [ ]', (done) => {
      chai.request(app)
        .put('/pet')
        .send([ ])
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });
    
    it('should not put a pet - body = null', (done) => {
      chai.request(app)
        .put('/pet')
        .send(null)
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });
  });
});