// Import the dependencies for testing
const app = require('../../index');
const chai = require('chai');
const chaiHttp = require('chai-http');

// Configure chai
chai.use(chaiHttp);
chai.should();

describe('Pet', () => {
  describe('GET / ALL', () => {
    it('should get all pet', (done) => {
      chai.request(app)
        .get('/pet/all')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          done();
        });
    });

    it('should get all pet with pagination', (done) => {
      const pagination = { page: 2, limit: 5 };
      chai.request(app)
        .get('/pet/all')
        .query({ ...pagination })
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          done();
        });
    });
  });
});