// Import the dependencies for testing
const app = require('../../index');
const chai = require('chai');
const chaiHttp = require('chai-http');

// Configure chai
chai.use(chaiHttp);
chai.should();

describe('Pet', () => {
  describe('DELETE /', () => {
    // it('should delete pet', (done) => {
    //   const id = '5fda1d2748a27f94dd6a06fc'; // valid mongo_id
    //   chai.request(app)
    //     .delete(`/pet`)
    //     .query({id})
    //     .end((err, res) => {
    //       res.should.have.status(200);
    //       done();
    //     });
    // });

    it('should not delete pet - Invalid id', (done) => {
      const id = 'invalid';
      chai.request(app)
        .get(`/pet`)
        .query({id})
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });
    
    it('should not delete pet - null id', (done) => {
      const id = null;
      chai.request(app)
        .get(`/pet`)
        .query({id})
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });
  });
});