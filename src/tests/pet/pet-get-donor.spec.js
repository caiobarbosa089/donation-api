// Import the dependencies for testing
const app = require('../../index');
const chai = require('chai');
const chaiHttp = require('chai-http');

// Configure chai
chai.use(chaiHttp);
chai.should();

describe('Pet', () => {
  describe('GET / DONOR', () => {
    it('should get all donors pet', (done) => {
      const donor_id = '5fdab93e09d6342938e58318'; // valid mongo_id
      chai.request(app)
        .get('/pet/donor')
        .query(donor_id)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          done();
        });
    });

    it('should get all donors pet with pagination', (done) => {
      const donor_id = '5fdab93e09d6342938e58318'; // valid mongo_id
      const pagination = { page: 2, limit: 5 };
      chai.request(app)
        .get('/pet/donor')
        .query({ donor_id, ...pagination })
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          done();
        });
    });
  });
});