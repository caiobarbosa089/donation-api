// Import the dependencies for testing
const app = require('../../index');
const chai = require('chai');
const chaiHttp = require('chai-http');

// Configure chai
chai.use(chaiHttp);
chai.should();

describe('Pet', () => {
  describe('GET /', () => {
    it('should get pet', (done) => {
      const id = '5fdab997c89cca3c7cb0c3bc'; // valid mongo_id
      chai.request(app)
        .get(`/pet`)
        .query({id})
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          done();
        });
    });

    it('should get pet with pagination', (done) => {
      const id = '5fdab997c89cca3c7cb0c3bc'; // valid mongo_id
      const pagination = { page: 2, limit: 5 };
      chai.request(app)
        .get(`/pet`)
        .query({id, ...pagination})
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          done();
        });
    });

    it('should not get pet - Invalid id', (done) => {
      const id = 'invalid';
      chai.request(app)
        .get(`/pet`)
        .query({id})
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });
    
    it('should not get pet - null id', (done) => {
      const id = null;
      chai.request(app)
        .get(`/pet`)
        .query({id})
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });
  });
});