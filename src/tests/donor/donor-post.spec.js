// Import the dependencies for testing
const app = require('../../index');
const chai = require('chai');
const chaiHttp = require('chai-http');

// Configure chai
chai.use(chaiHttp);
chai.should();

describe('Donor', () => {
  describe('POST /', () => {
    it('should post a donor', (done) => {
      const body = {
        name: "John Doe",
        contact: "+551191234567",
        adress: "Brazil, São Paulo - SP"
      };
      chai.request(app)
        .post('/donor')
        .send(body)
        .end((err, res) => {
          res.should.have.status(201);
          res.body.should.be.a('object');
          done();
        });
    });

    it('should not post a donor - body = { }', (done) => {
      chai.request(app)
        .post('/donor')
        .send({ })
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });

    it('should not post a donor - body = [ ]', (done) => {
      chai.request(app)
        .post('/donor')
        .send([ ])
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });
    
    it('should not post a donor - body = null', (done) => {
      chai.request(app)
        .post('/donor')
        .send(null)
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });
  });
});