// Import the dependencies for testing
const app = require('../../index');
const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');

// Configure chai
chai.use(chaiHttp);
chai.should();

describe('Donor', () => {
  describe('PUT /', () => {
    it('should put a donor', (done) => {
      const body = {
        _id: '5fdab93e09d6342938e58318', // valid mongo_id
        name: "John Doe",
        contact: "+551191234567",
        adress: "Brazil, São Paulo - SP"
      };
      chai.request(app)
        .put('/donor')
        .send(body)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          done();
        });
    });

    it('should not put a donor - id not created', (done) => {
      const body = {
        _id: mongoose.Types.ObjectId(),
        name: "John Doe",
        contact: "+551191234567",
        adress: "Brazil, São Paulo - SP"
      };
      chai.request(app)
        .put('/donor')
        .send(body)
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });

    it('should not put a donor - invalid id', (done) => {
      const body = {
        _id: 'invalid',
        name: "John Doe",
        contact: "+551191234567",
        adress: "Brazil, São Paulo - SP"
      };
      chai.request(app)
        .put('/donor')
        .send(body)
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });

    it('should not put a donor - null id', (done) => {
      const body = {
        _id: null,
        name: "John Doe",
        contact: "+551191234567",
        adress: "Brazil, São Paulo - SP"
      };
      chai.request(app)
        .put('/donor')
        .send(body)
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });

    it('should not put a donor - body = { }', (done) => {
      chai.request(app)
        .put('/donor')
        .send({ })
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });

    it('should not put a donor - body = [ ]', (done) => {
      chai.request(app)
        .put('/donor')
        .send([ ])
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });
    
    it('should not put a donor - body = null', (done) => {
      chai.request(app)
        .put('/donor')
        .send(null)
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });
  });
});