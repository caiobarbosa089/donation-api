// Import the dependencies for testing
const app = require('../../index');
const chai = require('chai');
const chaiHttp = require('chai-http');

// Configure chai
chai.use(chaiHttp);
chai.should();

describe('Donor', () => {
  describe('GET / ALL', () => {
    it('should get all donor', (done) => {
      chai.request(app)
        .get('/donor/all')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          done();
        });
    });

    it('should get all donor with pagination', (done) => {
      const pagination = { page: 2, limit: 5 };
      chai.request(app)
        .get('/donor/all')
        .query({ ...pagination })
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          done();
        });
    });
  });
});