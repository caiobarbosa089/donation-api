// Import the dependencies for testing
const app = require('../../index');
const chai = require('chai');
const chaiHttp = require('chai-http');

// Configure chai
chai.use(chaiHttp);
chai.should();

describe('Donor', () => {
  describe('GET /', () => {
    it('should get donor', (done) => {
      const id = '5fdab93e09d6342938e58318'; // valid id
      chai.request(app)
        .get(`/donor`)
        .query({id})
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          done();
        });
    });

    it('should not get donor - Invalid id', (done) => {
      const id = 'invalid';
      chai.request(app)
        .get(`/donor`)
        .query({id})
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });
    
    it('should not get donor - null id', (done) => {
      const id = null;
      chai.request(app)
        .get(`/donor`)
        .query({id})
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });
  });
});