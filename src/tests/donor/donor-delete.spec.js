// Import the dependencies for testing
const app = require('../../index');
const chai = require('chai');
const chaiHttp = require('chai-http');

// Configure chai
chai.use(chaiHttp);
chai.should();

describe('Donor', () => {
  describe('DELETE /', () => {
    // it('should delete donor', (done) => {
    //   const id = '5fda34e6179cd5af52e96db8'; // valid mongo_id
    //   chai.request(app)
    //     .delete(`/donor`)
    //     .query({id})
    //     .end((err, res) => {
    //       res.should.have.status(200);
    //       done();
    //     });
    // });

    it('should not delete donor - Invalid id', (done) => {
      const id = 'invalid';
      chai.request(app)
        .get(`/donor`)
        .query({id})
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });
    
    it('should not delete donor - null id', (done) => {
      const id = null;
      chai.request(app)
        .get(`/donor`)
        .query({id})
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });
  });
});